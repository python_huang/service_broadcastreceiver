package com.example.service_broadcastreceiver_kotlin

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

/*當按下Start按鈕, testActivity會通過意圖(Intent)啟動testService.
在testService的onStartCommand()創建並啟動一個線程(thread), 在線程裡面重複發送廣播(sendBroadcast()),
把當前系統時間送給testActivity 的MyReceiver.
當MyReceiver接收到, 會把接收到的時間和新的系統時間比較, 並把延遲時間顯示出來.

當按下Stop按鈕, testActivity會發送廣播送給testService 的 TestServiceReceiver.
當TestServiceReceiver接收到, 便會停止服務.*/

class TestActivity : Activity() {

    internal lateinit var textData: TextView
    private lateinit var myReceiver: MyReceiver

    /**
     * Called when the activity is first created.
     */
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textData = findViewById<View>(R.id.data) as TextView
        val buttonStart = findViewById<View>(R.id.start) as Button
        val buttonStop = findViewById<View>(R.id.stop) as Button
        buttonStart.setOnClickListener{
            val intent = Intent(this@TestActivity, TestService::class.java)
            startService(intent)
        }
        buttonStop.setOnClickListener{
            val intent = Intent()
            intent.action = TestService.MY_ACTION
            intent.putExtra("RQS", RQS_STOP_SERVICE)
            sendBroadcast(intent)
        }
    }

    override fun onStart() {
        // TODO Auto-generated method stub
        myReceiver = MyReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction(MY_ACTION)
        registerReceiver(myReceiver, intentFilter)
        super.onStart()
    }

    override fun onStop() {
        // TODO Auto-generated method stub
        unregisterReceiver(myReceiver)
        super.onStop()
    }

    private inner class MyReceiver : BroadcastReceiver() {

        override fun onReceive(arg0: Context, arg1: Intent) {
            // TODO Auto-generated method stub
            val timestamp = arg1.getLongExtra("timestamp", 0)
            val curtime = System.currentTimeMillis()
            val delay = curtime - timestamp
            textData.text = (timestamp.toString()
                    + " : " + curtime.toString()
                    + " delay " + delay.toString()
                    + "(ms)")
        }

    }

    companion object {
        internal val MY_ACTION = "testActivity.MY_ACTION"

        val RQS_STOP_SERVICE = 1
    }

}