package com.example.service_broadcastreceiver_kotlin

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import android.widget.Toast

class TestService : Service() {

    internal lateinit var testServiceReceiver: TestServiceReceiver
    internal var running: Boolean = false
    val create = "TestServiceReceiver.onCreate"
    val startCommand = "TestServiceReceiver.onStartCommand"
    val receive = "TestServiceReceiver.onReceive RQS_STOP_SERVICE"

    override fun onCreate() {
        // TODO Auto-generated method stub
        Toast.makeText(baseContext, create, Toast.LENGTH_LONG).show()
        testServiceReceiver = TestServiceReceiver()
        super.onCreate()
    }

    override fun onBind(arg0: Intent): IBinder? {
        // TODO Auto-generated method stub
        return null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        // TODO Auto-generated method stub
        Toast.makeText(baseContext, startCommand, Toast.LENGTH_LONG).show()
        val intentFilter = IntentFilter()
        intentFilter.addAction(MY_ACTION)
        registerReceiver(testServiceReceiver, intentFilter)
        running = true

        val myThread = MyThread()
        myThread.start()

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        // TODO Auto-generated method stub
        this.unregisterReceiver(testServiceReceiver)
        super.onDestroy()
    }

    inner class MyThread : Thread() {

        override fun run() {
            // TODO Auto-generated method stub

            // TODO Auto-generated method stub
            while (running) {
                try {
                    sleep(1000)
                } catch (e: InterruptedException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                val intent = Intent()
                intent.action = TestActivity.MY_ACTION
                intent.putExtra("timestamp", System.currentTimeMillis())
                sendBroadcast(intent)
            }
        }

    }

    inner class TestServiceReceiver : BroadcastReceiver() {

        override fun onReceive(arg0: Context, arg1: Intent) {
            // TODO Auto-generated method stub
            val rqs = arg1.getIntExtra("RQS", 0)
            if (rqs == TestActivity.RQS_STOP_SERVICE) {
                Toast.makeText(baseContext, receive, Toast.LENGTH_LONG).show()
                running = false
                stopSelf()
            }
        }
    }

    companion object {
        internal val MY_ACTION = "testService.MY_ACTION"
    }

}